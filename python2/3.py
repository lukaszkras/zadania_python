#!/usr/bin/python

from xml.dom.minidom import parse
import xml.dom.minidom

plik = xml.dom.minidom.parse("ja.xml")
zbior = plik.documentElement

tag = zbior.getElementsByTagName( 'Device' )
print("Urzadzenia:")
for i in tag:
	print ("\n")
	nazwa=i.getElementsByTagName('name')[0].childNodes[0].data
	print("Nazwa: %s" % nazwa)	
	typ=i.getElementsByTagName('type')[0].childNodes[0].data
	print("Typ: %s" % typ)
	id=i.getElementsByTagName('id')[0].childNodes[0].data
	print("ID: %s" % id)
	wartosc=i.getElementsByTagName('value')[0].childNodes[0].data
	print("Wartosc: %s" % wartosc)
	jednostka=i.getElementsByTagName('unit')[0].childNodes[0].data
	print("Jednostka: %s" % jednostka)
	
tag[0].getElementsByTagName('unit')[0].firstChild.replaceWholeText( 'test' )
fileOut = open("testOutput.xml", "w")
zbior.writexml(fileOut)
fileOut.close()
