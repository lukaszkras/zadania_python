#!/usr/bin/python

import math

class Complex:
	r = 0
	i = 0
	def __init__(self,real = 0,imag = 0):
		self.r = real
		self.i = imag
	
	def str(self):
		if self.i >= 0:
			text = "Liczba zespolona: %f + %fi" %(self.r,self.i)
		else:
			text = "Liczba zespolona: %f - %fi" %(self.r,abs(self.i))
		return text
		
	def add(self,other):
		r = self.r+other.r
		i = self.i+other.i
		if i >= 0:
			text = "Wynik dodawania: %f + %fi" %(r,i)
		else:
			text = "Wynik dodawania: %f - %fi" %(r,abs(i))
		return text
		
	def sub(self,other):
		r = self.r-other.r
		i = self.i-other.i
		if i >= 0:
			text = "Wynik odejmowania: %f + %fi" %(r,i)
		else:
			text = "Wynik odejmowania: %f - %fi" %(r,abs(i))
		return text
		
	def abs(self):
		abs = math.sqrt((self.r*self.r)+(self.i*self.i))
		text = "Modul liczby zespolonej: %f" %(abs)
		return text
		
	def mux(self,other):
		r = (self.r*other.r)-(self.i*other.i)
		i = (self.r*other.i)+(self.i*other.r)
		if i >= 0:
			text = "Wynik mnozenia: %f + %fi" %(r,i)
		else:
			text = "Wynik mnozenia: %f - %fi" %(r,abs(i))
		return text
		
	def con(self):
		r = self.r
		i = -self.i
		if i >= 0:
			text = "Sprzezenie liczby zespolonej: %f + %fi" %(r,i)
		else:
			text = "Sprzezenie liczby zespolonej: %f - %fi" %(r,abs(i))
		return text
		
	def neg(self):
		r = -self.r
		i = -self.i
		if i >= 0:
			text = "Liczba odwrotna: %f + %fi" %(r,i)
		else:
			text = "Liczba odwrotna: %f - %fi" %(r,abs(i))
		return text
		
		
a = Complex(1.0,1.0)
b = Complex(1.0,-1.0)
print(a.str())
print(a.add(b),"\n",a.sub(b),"\n")
print(a.abs(),"\n",a.mux(b),"\n")
print(a.con(),"\n",a.neg())