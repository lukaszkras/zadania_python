#!/usr/bin/python

import threading
import time
import random

def filozof( numer, widelce ):
	for i in range( 5 ):
		print( "Filozof %d zaczyna jedzenie \n\r" % numer )
		
		zjadlem = 0
		
		while zjadlem == 0:
			if widelce[numer%5] == 0:
				widelce[numer%5] = 1
				time.sleep( 2 )
				if widelce[(numer+1)%5] == 0:
					widelce[(numer+1)%5] = 1
					print( "Filozof %d teraz je \n\r" % numer )
					time.sleep( 10 )
					zjadlem = 1
					
					widelce[numer%5] = 0
					widelce[(numer+1)%5] = 0
				else:
					widelce[numer%5] = 0
					print( "Filozof %d teraz czeka \n\r" % numer )
					time.sleep( int( random.random() * 5 ) )			
		
	print( "Koniec %d\n" % numer )
	
#------------------------------------------------	
widelce = []
for i in range(5):
    widelce.append( 0 )

p1 = threading.Thread( target=filozof, args=(0, widelce) )
p2 = threading.Thread( target=filozof, args=(1, widelce) )
p3 = threading.Thread( target=filozof, args=(2, widelce) )
p4 = threading.Thread( target=filozof, args=(3, widelce) )
p5 = threading.Thread( target=filozof, args=(4, widelce) )

p1.start()
p2.start()
p3.start()
p4.start()
p5.start()

p1.join()
p2.join()
p3.join()
p4.join()
p5.join()

print( "\n\rFilozofowie skończyli jeść\n\r" )

