#!/usr/bin/python

import math

a,b,c = input("Podaj wspolczynniki a,b,c funkcji: ").split()

a = float(a)
b = float(b)
c = float(c)

delta = (b * b) - (4 * a * c)

if delta > 0:
	x1 = (-b - math.sqrt(delta))/(2*a)
	x2 = (-b + math.sqrt(delta))/(2*a)
	print("Delta wynosi ",delta,", funkcja ma dwa pierwiastki: x1=",x1,", x2=",x2)
elif delta == 0:
	x = -b/(2*a)
	print("Delta wynosi 0, funkcja ma tylko jeden pierwiastek: ",x)
else:
	print("Delta wynosi ",delta,", funkcja nie ma pierwiastkow.")