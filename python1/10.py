#!/usr/bin/python

print("Podaj nazwe pliku wejsciowego: ")
plik1 = input()
print("Podaj nazwe pliku wyjsciowego: ")
plik2 = input()

plikwe = open(plik1,'r')
odczyt = plikwe.read()
print(odczyt)
plikwe.close()

odczyt = odczyt.replace(" i "," oraz ")
odczyt = odczyt.replace(' oraz',' i')
odczyt = odczyt.replace(' nigdy',' prawie nigdy')
odczyt = odczyt.replace(' dlaczego',' czemu')

print("\n",odczyt)

plikwy = open(plik2,'w')
plikwy.truncate()
plikwy.write(odczyt)
plikwy.close

plikwy = open(plik2,'r')
odczyt = plikwy.read()
print(odczyt)
plikwy.close()