#!/usr/bin/python

import random

numbers = random.sample(range(1000),50)
print("Wylosowane numery: \n",numbers)
dosort = numbers

for i in range(len(numbers)):
	for j in range(len(numbers) - 1):
		if numbers[j] < numbers[j+1]:
			tmp = numbers[j]
			numbers[j] = numbers[j+1]
			numbers[j+1] = tmp
			
print("Posortowane numery: \n",numbers)

print("Posortowane przez wbudowana funkcje: \n",sorted(dosort,reverse=True))