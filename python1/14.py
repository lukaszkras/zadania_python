#!/usr/bin/python

import math
import random

def wyznacznik(a):
	if len(a) == 1:
			return a[0][0]
	else:
			detA = 0
			for i in range( len(a) ):
				detB = pow( -1, 2+i ) * a[0][i]
				b = []
				for j in range( len(a)-1 ):
					b.append( [] )
					for k in range( len(a[0]) ):
						if k != i:
							b[j].append( a[j+1][k] )
				
				detA = detA + detB * wyznacznik( b )
			return detA
			
rozmiar = 0
macierz = []
while rozmiar == 0:
	rozmiar = int(random.random()*5)

for i in range(rozmiar):
	macierz.append([])
	for j in range(rozmiar):
		macierz[i].append(int(random.random()*10))
		
wyznacznik = wyznacznik(macierz)
print("Rozmiar macierzy: ",rozmiar,"\nMacierz: ",macierz,"\n\nWyznacznik macierzy: ",wyznacznik)